Using the digital elevation data
================================

Data source
-----------
We obtained the digital elevation data from Geosciences Australia at the
following URLs:

http://www.ga.gov.au/products/servlet/controller?event=DEFINE_PRODUCTS
http://www.ga.gov.au/products/servlet/controller?event=PRODUCT_SELECTION&theme=Digital+Elevation+Data&keyword=

The scripts are flexible enough that the resolution of the digital elevation
data does not matter. The scripts will get the origin and pixel size from
the geospatial information in the images, and calculated longitudes and
latitudes correctly.

Note that all elevation heights are in mean metres above sea level.

Technologies used, and dependencies
-----------------------------------
The python bindings to GDAL were used to read the BIL files of digital 
elevation data from GeoScience Australia. MongoDB was used as the
database, with the pymongo python bindings used to insert data into the
database and convert the raw elevation data to GeoJSON format for
geospatial indexing.

insert_all_bil_files.sh
-----------------------
This shell script inserts all of the BIL files in the
globalmaps2001/Raster/elevation/ subdirectory into the MongoDB database,
using the insert_bil_file.py script.

insert_bil_file.py
------------------
The insert_bil_file.py uses the Python GDAL bindings to read the digital
elevation data from the Banded InterLeaved format image files and insert them
into a MongoDB database. The script reads the image file passed as the
first command line argument, and inserts it into the elevation.raw_elevation
collection.

convert_points_to_GeoJSON.py
----------------------------
This script converts the raw_elevation collection in the elevation database
in MongoDB to GeoJSON points, which can then be geospatially indexed using
the MongoDB command:
db.GeoJSON_elevation.ensureIndex( { "location": "2dsphere" } )
as detailed here
http://docs.mongodb.org/manual/core/geospatial-indexes/

Performance
-----------
On my Macbook Pro with 16 gig. of RAM, the import process took roughly forty
minutes in total, and the geospatial indexing took about a quarter of an
hour. Both processes could have been sped up if I've rewritten the code to
use multiple processes or threads.

Once the geospatial indexing was done, the thirty million points in our database
could be searched for the nearest point to a longitude, latitude pair in
a fraction of a second. MongoDB's geospatial indexing makes clever use of
BTrees, and the performance is very impressive!

Acknowledgements
----------------
* Thank you to Anthony Baxter from Google for the advice on the digital
elevation data and the Google Maps APIs.

* An especially big thank you to Stennie from 10gen for the clueful and
timely advice on MongoDB, and recommending it to us in the first place.

* Thanks also to Rhydwyn McGuire for helping me work out what units the
digital elevation heights were actually in by chasing down documentation
related to the documentation that Geoscience Australia supplied.

- Mark Greenaway, certifiedwaif@gmail.com
