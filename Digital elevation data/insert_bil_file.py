import gdal
from gdalconst import *
import struct
import sys
import pymongo
import numpy

def main():
	# Open the file
	#bil_filename = sys.argv[1]
	bil_filename = "globalmap2001/Raster/elevation/elwell.bil"
	print "Inserting %s into MongoDB" % bil_filename
	dataset = gdal.Open( bil_filename, GA_ReadOnly )
	if dataset is None:
		raise Error("%s could not be loaded")
	print 'Driver: ', dataset.GetDriver().ShortName,'/', \
		dataset.GetDriver().LongName
	print 'Size is ',dataset.RasterXSize,'x',dataset.RasterYSize, \
		'x',dataset.RasterCount
	print 'Projection is ',dataset.GetProjection()
	    
	geotransform = dataset.GetGeoTransform()
	if not geotransform is None:
		print 'Origin = (',geotransform[0], ',',geotransform[3],')'
		print 'Pixel Size = (',geotransform[1], ',',geotransform[5],')'
		origin = geotransform[0], geotransform[3]
		pixel_size = geotransform[1], geotransform[5]
	
	# Get first raster band
	band = dataset.GetRasterBand(1)
	
	print 'Band Type=',gdal.GetDataTypeName(band.DataType)
	
	bandMin = band.GetMinimum()
	bandMax = band.GetMaximum()
	if bandMin is None or bandMax is None:
		(bandMin,bandMax) = band.ComputeRasterMinMax(1)
		print 'Min=%.3f, Max=%.3f' % (bandMin,bandMax)
	
	if band.GetOverviewCount() > 0:
		print 'Band has ', band.GetOverviewCount(), ' overviews.'
	
	if not band.GetRasterColorTable() is None:
		print 'Band has a color table with ', \
		band.GetRasterColorTable().GetCount(), ' entries.'
	
	# Read scanlines and insert into the database
	conn = pymongo.Connection()
	conn.write_concern = {"w":0}
	db = conn.elevation
	origin_long, origin_lat = origin
	pixel_size_long, pixel_size_lat = pixel_size


	for y in range(dataset.RasterYSize):
		scanline = band.ReadRaster( 0, y, band.XSize, 1, \
		                                     band.XSize, 1, GDT_Float32 )
		heights = struct.unpack('f' * band.XSize, scanline)
		
		# Print first ten values in scanline, scratch head at their meaning ...
		#print tuple_of_floats[1:10]
		#print "These numbers are in metres"
		#print min(tuple_of_floats)
		#print max(tuple_of_floats)
		# Produce a CSV of longitude, latitude, height in metres
		x = numpy.arange(0, len(heights))
		lat = origin_lat + y*pixel_size_lat
		longs = origin_long + x*pixel_size_long
		#insert (long, lat, tuple_of_floats[x])
		for idx in  range(len(heights)):
			db.raw_elevation.insert({"longitude":longs[idx], "latitude":lat, "elevation":heights[idx]})
			
	print "Insertion finished for %s" % bil_filename
		
if __name__ == "__main__":
	main()
