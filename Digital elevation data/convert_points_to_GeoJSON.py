'''
Created on Jun 1, 2013

@author: mark
'''
import pymongo

def main():
    conn = pymongo.Connection()
    counter = 0
    for raw_entry in conn.elevation.raw_elevation.find():
        if counter % 1e5 == 0:
            print "Processed %d entries" % counter
        conn.elevation.GeoJSON_elevation.insert({
            "location" : {"type" : "Point",
                          "coordinates" : [
                            raw_entry["longitude"],
                            raw_entry["latitude"]
                            ]},
            "elevation" : raw_entry["elevation"]
        })
        counter += 1
        
if __name__ == "__main__":
    main()