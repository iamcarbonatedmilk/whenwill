var coords = {
							 latitude: -33.8673111,
							 longitude: 151.2039715
						 };

if (navigator.geolocation) {
	navigator.geolocation.getCurrentPosition(showPosition);
} else {
	x.innerHTML="Geolocation is not supported by this browser.";
}

function showPosition(position){
  coords = position.coords;
}


function buildStreet(){
	d3.select("#street-button").remove();

	var w = $("#content").width(),
			h = $("#content").height();


	console.log($("#content").width());

	var vis =	d3.select("#streetview").append("svg:svg")
			.attr("id","vis")
			.attr("width", w)
			.attr("height", h);
//		.attr("style","padding-top: 20px; margin: 0 20px 100px 20px");		// use a variable based on widow size?

	street = vis.append("svg:image")
		.attr("style","height: 100%;")
		.attr("xlink:href", "http://maps.googleapis.com/maps/api/streetview?size=640x640&location="+coords.latitude+","+coords.longitude+"&fov=90&heading=235&pitch=10&sensor=false")
		.attr("id", "street-pic")
		.attr("width", 640)
		.attr("height", 640);

	vis.append("svg:g")
			.attr("id", "sview-g")
		.append("foreignObject")
	  .append("xhtml:canvas")
			.attr("width", w)
			.attr("height", h)
			.attr("id", "world");
		
	  	wave = new Wave();
			wave.Initialize('world');


	elevation = 2;
	url = "http://maps.googleapis.com/maps/api/elevation/json?locations="+coords.latitude+","+coords.longitude+"&sensor=false";
	$.get(url,function(json){
		console.log(json);
		elevation = json.results[0].elevation;

		d3.select("#doom").html("Your current location is "+Math.floor(elevation)+" meters above today's sea level. But in the year "+Math.floor(getYear(elevation))+" this area will be under water.");

		console.log(getYear(elevation));
	});


	



//	console.log($("#streetview").width());


/*
 d3.select("#street-tag").remove();
	var w = $("#content").width(),
			h = $("#content").height();

	console.log($("#content").width());


	var sView =	d3.select("#streetview")
	
	sView.append("xhtml:image")
		.attr("src", "http://maps.googleapis.com/maps/api/streetview?size=640x640&location="+coords.latitude+",%20"+coords.longitude+"&fov=90&heading=235&pitch=10&sensor=false")
		.attr("style","padding-top: 20px; margin: 0 20px 100px 20px")
		.attr("id", "street-pic");
		
		
	sView.append("svg:svg")
			.attr("style","position:absolute;")
			.attr("id","vis")
			.attr("width", w)
			.attr("height", h)
		.append("svg:g")
			.attr("id", "sview-g")
			.append("foreignObject")
			.append("xhtml:canvas")
			.attr("width", w)
			.attr("height", h)
			.attr("id", "world");

	console.log($("#streetview").width());

	  	wave = new Wave();
			wave.Initialize('world');
*/
}


function getYear(elevation){
	var year = 2088;

	e = elevation * 100;

	if (e < 144) {
	//this is the equation: year-1990 = 0.0111x2 + 0.0685*elevation - 0.0796
	//who cares? the error is larger than 144 cm.
		year = 2087
	} else if (e < 355) {
		year = ((e-142.14)/2.1366)+2099;
	} else {
		year = ((e-356)/1.5229)+2199;
	}

	return year;
}























