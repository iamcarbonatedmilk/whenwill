# Author: Rhydwyn McGuire <rhydwyn.beta@gmail.com>
X_axis = as.data.frame(rbind(c(0,230), c(500,10)))
colnames(X_axis) = c("inMeters", "inPixels")
convert_sea_level = lm(inMeters ~ inPixels , X_axis )
summary(convert_sea_level)
predict(convert_sea_level, 190)

in_meters <- function(x)
{
  return(522.727 - 2.273*x)
}

predict_1990 = c(0, 0, 0)
predict_2050 = in_meters(c(221, 215, 210))
predict_2100 = in_meters(c(207, 190, 166.5))
predict_2200 = in_meters(c(165.5, 124, 72.5))
predict_2300 = in_meters(c(117.5, 65, 5.5))

#this was done in a weird it should be rewritten




predict_all = rbind(predict_1990, predict_2050, predict_2100,predict_2200,predict_2300 )

Predictions = as.data.frame(cbind(c(1990, 2050, 2100, 2200, 2300),predict_all))



colnames(Predictions) = c("year", "low", "mid", "high")

Predictions$year_sq = (Predictions$year)^2

low_19x = lm(low ~ year + year_sq, Predictions[1:3,] )
low_21x = lm(low ~ year, Predictions[3:4,] )
low_22x = lm(low ~ year, Predictions[4:5,] )
mid_19x = lm(mid ~ year + year_sq, Predictions[1:3,] )
mid_21x = lm(mid ~ year, Predictions[3:4,] )
mid_22x = lm(mid ~ year, Predictions[4:5,] ) 
high_19x = lm(high ~ year + year_sq, Predictions[1:3,] )
high_21x = lm(high ~ year, Predictions[3:4,] )
high_22x = lm(high ~ year, Predictions[4:5,] ) 

years_19x = as.data.frame(1990:2099)
years_21x = as.data.frame(2100:2199)
years_22x = as.data.frame(2200:2300)

colnames(years_19x) = 'year'
colnames(years_21x) = 'year'
colnames(years_22x) = 'year'


years_19x$year_sq = (years_19x$year)^2
years_21x$year_sq  = (years_21x$year)^2
years_22x$year_sq  = (years_22x$year)^2


low = cbind(1990:2300, c(predict(low_19x, years_19x), predict(low_21x, years_21x), predict(low_22x, years_22x)))
colnames(low) = c('year', 'low_essimate')
mid = cbind(1990:2300, c(predict(mid_19x, years_19x),predict(mid_21x, years_21x), predict(mid_22x, years_22x)))
colnames(mid) = c('year', 'mid_essimate')
high = cbind(1990:2300, c(predict(high_19x, years_19x),predict(high_21x, years_21x), predict(high_22x, years_22x)))
colnames(mid) = c('year', 'high_essimate')




prediction_single_years = cbind(1990:2300, low[,2], mid[,2], high[,2])

plot(prediction_single_years[,1],prediction_single_years[,4], type = 'l' )
lines(prediction_single_years[,1],prediction_single_years[,3] )
lines(prediction_single_years[,1],prediction_single_years[,2] )

prediction_single_years = as.data.frame(prediction_single_years)

colnames(prediction_single_years) = c("year", "low estimate", "mid estimate", "high estimate")

write.csv(prediction_single_years, "prediction_single_years.csv")
summary(low_21x)
