// All Tomorrow's Parties -- server

//Meteor.publish("directory", function () {
//  return Meteor.users.find({}, {fields: {emails: 1, profile: 1}});
//});

Meteor.publish("GeoJSON_elevation", function () {
    return Meteor.GeoJSON_elevation.find({}, {fields: {elevation: 1, location: 1}});
})

//Meteor.publish("parties", function () {
//  return Parties.find(
//    {$or: [{"public": true}, {invited: this.userId}, {owner: this.userId}]});
//});
