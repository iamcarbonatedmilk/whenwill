// When Will My House Be Underwater -- client

//Elevation = new Meteor.Collection("raw_elevation");

Meteor.subscribe("GeoJSON_elevation");
Meteor.subscribe("co2");
Meteor.subscribe("suburb");
Meteor.subscribe("seaLevel");

// startup function -- create a dummy data point

Meteor.startup(function () {
   Deps.autorun(function () {
   });
}); 

// funcs

getElevation = function (lat, lon) {
  check(lat, Number);
  check(lon, Number);
  return geojson_elevation.findOne({}, {elevation: 1, _id: 0});
};

getContours = function (height) {
    check(height, Number);
    return Raw_elevation.find({elevation: {$near: height}}, {latitude: 1, longitude: 1, _id:0});
}

getCO2 = function (date) {
    check(year, Number);
    var datef = Math.floor(year);
    return CO2.find({year: datef}, {concentration: 1, _id: 0});
}

// 6 feet tides! Hard coded for awesome goodness.
getTime = function(elev) {
    return SeaLevel.findOne({"high estimate": {$gte : elev - 1.9}},{"high estimate": 1, _id: 0});
}

// more funcs for lazy kids

getTimeByPlace = function (lat, lon) {
    check(lat, Number);
    check(lon, Number);
    var temp = getElevation(lat, lon);
    return getTime(temp);
}
