var elevator = new google.maps.ElevationService();

function getElevation(lat,lon, callback) {

  var location =  [new google.maps.LatLng(lat, lon)];

  // Create a LocationElevationRequest object using the array's one value
    positionalRequest = { 'locations' : location}
  
  // Initiate the location request
  elevator.getElevationForLocations(positionalRequest, function(results, status) {
    if (status == google.maps.ElevationStatus.OK) {

      // Retrieve the first result
      if (results[0]) {
callback(results[0].elevation);
        // Open an info window indicating the elevation at the clicked position
        //alert("The elevation at this point is " + results[0].elevation + " meters.");
      } else {
        //alert("No results found");
      }
    } else {
      callback();
    }
  });
}
